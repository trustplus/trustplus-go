package main
import "flag"
import "net"
import "fmt"
import "log"
import "bitbucket.org/trustplus/trustplus-go"

var f_apiKey = flag.String("apikey", "", "Trust+ API key")
var f_endpoint = flag.String("endpoint", trustplus.DefaultEndpoint, "Trust+ API endpoint")
var f_insecureSkipVerify = flag.Bool("insecureskipverify", false, "Don't check server TLS certificate")
var f_ip    = flag.String("ip", "", "User IP address")
var f_email = flag.String("email", "", "User e. mail address")
var f_clientToken = flag.String("clienttoken", "", "Client token")
var f_expectClientToken = flag.Bool("expectclienttoken", false, "Expect client token?")

func main() {
  flag.Parse()

  if len(*f_clientToken) > 0 {
    *f_expectClientToken = true
  }

  trustplus.DefaultConfig.APIKey = *f_apiKey
  trustplus.DefaultConfig.Endpoint = *f_endpoint
  trustplus.DefaultConfig.InsecureSkipVerify = *f_insecureSkipVerify
  args := trustplus.CheckArgs{}

  var err error
  if len(*f_ip) > 0 {
    args.IP = net.ParseIP(*f_ip)
    if args.IP == nil {
      log.Fatalf("Invalid IP address: %+v", *f_ip)
    }
  }

  args.Email = *f_email
  args.ClientToken = *f_clientToken
  args.ExpectClientToken = *f_expectClientToken

  if args.IP == nil && args.Email == "" {
    log.Fatalf("You must specify at least an IP or e. mail address, or both.")
  }

  result, err := trustplus.Check(args)
  if err != nil {
    log.Fatalf("Check failed: %+v", err)
  }

  if result.Pass {
    fmt.Printf("PASS %v\n", result.Score)
  } else {
    fmt.Printf("FAIL %v\n", result.Score)
  }
}
