package trustplus
import "net"
import "net/http"
import "bytes"
import "errors"
import "crypto/tls"
//import "github.com/gorilla/rpc/v2"
import "github.com/gorilla/rpc/v2/json2"

var DefaultConfig Config

// Use the DefaultConfig to make a Check call.
func Check(args CheckArgs) (CheckResult, error) {
  return DefaultConfig.Check(args)
}

type Config struct {
  APIKey string // Your Trust+ API key
  Endpoint string // Defaults to "https://api.trust.plus/check"

  InsecureSkipVerify bool // Do not validate server's TLS certificate
}

// Arguments to a check call.
type CheckArgs struct {
  IP    net.IP // The IP address of the user
  Email string // The e. mail address of the user
  UserAgent string // The user's user agent string

  ClientToken string // For future use
  ExpectClientToken bool // For future use
}

type CheckTag struct {
  Score int `json:"score"`
}

// The result of a check call.
type CheckResult struct {
  Pass bool  `json:"pass"`  // Whether the user should be allowed
  Score int  `json:"score"` // Total risk score
  Tags map[string]CheckTag `json:"tags"`  // Risk tags
}

var ErrNoAPIKey = errors.New("must specify API key before calling")
var ErrMustSetExpectClientToken = errors.New("must set ExpectClientToken to true if you pass a client value")

const DefaultEndpoint = "https://api.trust.plus/check"

func (cfg *Config) Check(args CheckArgs) (result CheckResult, err error) {
  if cfg.Endpoint == "" {
    cfg.Endpoint = DefaultEndpoint
  }

  if cfg.APIKey == "" {
    err = ErrNoAPIKey
    return
  }

  m := map[string]interface{}{}
  if len(args.IP) > 0 && !args.IP.IsUnspecified() {
    m["ip"] = args.IP.String()
  }

  if len(args.Email) > 0 {
    m["email"] = args.Email
  }
  if len(args.UserAgent) > 0 {
    m["user-agent"] = args.UserAgent
  }

  if args.ExpectClientToken {
    m["client-token"] = args.ClientToken
  } else if args.ClientToken != "" {
    err = ErrMustSetExpectClientToken
    return
  }

  b, err := json2.EncodeClientRequest("trustplus.Check", m)
  if err != nil {
    return
  }

  req, err := http.NewRequest("POST", cfg.Endpoint, bytes.NewBuffer(b))
  if err != nil {
    return
  }

  req.Header.Set("Content-Type", "application/json-rpc")
  req.Header.Set("User-Agent", "trustplus-go/1.0")
  req.SetBasicAuth(cfg.APIKey, "")

  transport := *http.DefaultTransport.(*http.Transport)
  transport.TLSClientConfig = &tls.Config{
    MinVersion: tls.VersionTLS12,
    InsecureSkipVerify: cfg.InsecureSkipVerify,
  }

  client := *http.DefaultClient
  client.Transport = &transport

  res, err := client.Do(req)
  if err != nil {
    return
  }

  defer res.Body.Close()

  err = json2.DecodeClientResponse(res.Body, &result)
  if err != nil {
    return
  }

  //res.StatusCode
  return
}
